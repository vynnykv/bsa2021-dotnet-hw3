﻿using CoolParking.BL.Models;
using System.Linq;
using System;
using CoolParking.CMD.Models;

namespace CoolParking.CMD
{
    class Program
    {
        static void Main()
        {
            DisplayMainMenu();
            MainMenu();
        }

        static void DisplayMainMenu()
        {
            Console.WriteLine("Welcome to Cool Parking!\n\n" +
                "Enter a number what have to do:\n" +
                "1. Display the current balance of the parking.\n" +
                "2. Display the amount of money earned for the current period.\n" +
                "3. Display the number of free parking spaces.\n" +
                "4. Display all parking Transactions for the current period.\n" +
                "5. Display the transaction history.\n" +
                "6. Display the list of transports in the parking lot.\n" +
                "7. Put the transport in the parking lot.\n" +
                "8. Remove the transport from the parking.\n" +
                "9. Top up the balance of a transport.\n" +
                "10. Display transport by id.\n" +
                "11. Clear console.\n" +
                "12. Exit program.\n");
        }

        static void MainMenu()
        {
            Console.Write("Your choice: ");
            string choice = Console.ReadLine();
            try
            {
                switch (choice)
                {
                    case "1":
                        Console.WriteLine($"Current balance is {HTTPMethods.GetParkingHttp("balance").Result}");
                        break;
                    case "2":
                        Console.WriteLine($"The amount of money earned for the current period: {HTTPMethods.GetLastTransactions().Result.Sum(t => t.Sum)}");
                        break;
                    case "3":
                        Console.WriteLine($"Amount free place of parking is {HTTPMethods.GetParkingHttp("freePlaces").Result} out of {HTTPMethods.GetParkingHttp("capacity").Result}");
                        break;
                    case "4":
                        {
                            var transactions = HTTPMethods.GetLastTransactions().Result;
                            Console.WriteLine("The history of transactions for current period:");
                            foreach (var transaction in transactions)
                                Console.WriteLine($"VehicleId: {transaction.VehicleId} Sum: {transaction.Sum} TransactionTime: {transaction.TransactionTime:T}");
                            break;
                        }
                    case "5":
                        {
                            Console.WriteLine("Transactions history:");
                            var history = HTTPMethods.GetAllTransactions().Result;
                            Console.WriteLine(history);
                            break;
                        }
                    case "6":
                        {
                            Console.WriteLine("The transports in the parking lot:");
                            var transports = HTTPMethods.GetVehicles().Result;
                            foreach (var transport in transports)
                                Console.WriteLine($"{transport.Id}: {transport.VehicleType.ToString()} Balance: {transport.Balance}");
                            break;
                        }
                    case "7":
                        {
                            Console.WriteLine("Enter transport id, type of transport and amount of money:");
                            Console.Write("Vehicle id: ");
                            var id = Console.ReadLine();
                            int type;
                            Console.Write("Type of transport(1 - \"PassengerCar\", 2 - \"Truck\", 3 - \"Bus\", 4 - \"Motorcycle\"): ");
                            while (!int.TryParse(Console.ReadLine(), out type) || (type > 3 || type < 0))
                            {
                                Console.WriteLine("Type of transport must be 1-4");
                                Console.Write("Type of transport(1 - \"PassengerCar\", 2- \"Truck\", 3 - \"Bus\", 4 - \"Motorcycle\"): ");
                            }
                            Console.Write("Your money: ");
                            decimal money;
                            decimal.TryParse(Console.ReadLine(), out money);
                            if (HTTPMethods.AddVehicle(new VehicleViewModel() { Id = id, VehicleType = (VehicleType)type, Balance = money }).Result)
                                Console.WriteLine("Thanks for parking your transport");
                            else
                                Console.WriteLine("Sth went wrong...");
                            break;
                        }
                    case "8":
                        {
                            Console.WriteLine("Enter the id of transport:");
                            string id = Console.ReadLine();
                            if(HTTPMethods.RemoveVehicle(id).Result)
                                Console.WriteLine("Have a nice day!");
                            else Console.WriteLine("Sth went wrong...");
                            break;
                        }
                    case "9":
                        {
                            Console.Write("Enter the id of transport: ");
                            string id = Console.ReadLine();
                            Console.Write("Sum: ");
                            decimal money;
                            while (!decimal.TryParse(Console.ReadLine(), out money))
                            {
                                Console.WriteLine("Your input is wrong, try again");
                                Console.Write("Sum: ");
                            }
                            if(HTTPMethods.TopUpVehicle(id, money).Result)
                                Console.WriteLine($"Thanks, current balance: {HTTPMethods.GetVehicleById(id).Result.Balance}");
                            else 
                                Console.WriteLine("Sth went wrong...");
                            break;
                        }

                    case "10":
                        {
                            Console.Write("Enter the id of transport: ");
                            string id = Console.ReadLine();
                            var vehicle = HTTPMethods.GetVehicleById(id).Result;
                                Console.WriteLine($"VehicleId: {vehicle.Id} VehicleType: {vehicle.VehicleType.ToString() } Balance: {vehicle.Balance}");
                            break;
                        }

                    case "11":
                        Console.Clear();
                        break;

                    case "12":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Entered incorrect number, try again");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine();
                DisplayMainMenu();
                MainMenu();
            }
        }
    }
}
