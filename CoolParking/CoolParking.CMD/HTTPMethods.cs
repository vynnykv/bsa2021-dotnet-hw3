﻿using CoolParking.CMD.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoolParking.CMD
{
    public static class HTTPMethods
    {
        private static readonly HttpClient Client  = new HttpClient();

        private static readonly string baseUrl = new string(@"https://localhost:44306/api/"); 

        public static async Task<string> GetParkingHttp(string lastEndpoint)
        {
            var response = await Client.GetAsync(baseUrl + $"parking/{lastEndpoint}");
            return (await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<VehicleViewModel>> GetVehicles()
        {
            var response = await Client.GetAsync(baseUrl + "vehicles");
            return JsonSerializer.Deserialize<IEnumerable<VehicleViewModel>>(response.Content.ReadAsStringAsync().Result);
        }

        public static async Task<VehicleViewModel> GetVehicleById(string id)
        {
            var response = await Client.GetAsync(baseUrl + $"vehicles/{id}");
            string vehicle = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<VehicleViewModel>(vehicle);
        }

        public static async Task<bool> AddVehicle(VehicleViewModel vehicle)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(vehicle);
            var data = new StringContent(json,Encoding.UTF8, "application/json");
            var response = await Client.PostAsync(baseUrl + "vehicles", data);
            return response.IsSuccessStatusCode;
        }

        public static async Task<bool> RemoveVehicle(string id)
        {
            var response = await Client.DeleteAsync(baseUrl + $"vehicles/{id}");
            return response.IsSuccessStatusCode;
        }

        public static async Task<TransactionViewModel[]> GetLastTransactions()
        {
            var response = await Client.GetAsync(baseUrl + "transactions/last");
            return JsonSerializer.Deserialize<TransactionViewModel[]>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<string> GetAllTransactions()
        {
            var response = await Client.GetAsync(baseUrl + "transactions/all");
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<bool> TopUpVehicle(string id, decimal sum)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(new { VehicleId = id, Sum = sum});
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await Client.PutAsync(baseUrl + "transactions/topUpVehicle", data);
            return response.IsSuccessStatusCode;
        }
    }
}
