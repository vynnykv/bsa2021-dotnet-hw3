﻿using System;
using System.Text.Json.Serialization;

namespace CoolParking.CMD.Models
{
    public class TransactionViewModel
    {
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")]
        public DateTime TransactionTime { get; set; }
    }
}
