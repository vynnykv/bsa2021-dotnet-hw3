﻿using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Models
{
    public class TopUpVehicleDto
    {
        [JsonPropertyName("id")]
        public string VehicleId { get; set; }
        [JsonPropertyName("Sum")]
        public decimal Sum { get; set; }
    }
}
