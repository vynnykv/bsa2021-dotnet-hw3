﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService) =>
            _parkingService = parkingService;

        [Route("last")]
        [HttpGet]
        public ActionResult<IReadOnlyCollection<TransactionInfo>> GetLastTransactions() =>
            Ok(_parkingService.GetLastParkingTransactions());

        [Route("all")]
        [HttpGet]
        public ActionResult<string> AllTransactions()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] TopUpVehicleDto topUp)
        {
            if (topUp.VehicleId is null || !Vehicle.IsValidId(topUp.VehicleId))
                return BadRequest("Id is wrong");
            if (topUp.Sum <= 0)
                return BadRequest("The sum can not be less than zero");
            Vehicle vehicle = _parkingService.GetVehicleById(topUp.VehicleId);
            if (vehicle == null)
                return NotFound("Vehicle with this id did not found");
            _parkingService.TopUpVehicle(topUp.VehicleId, topUp.Sum);
            return Ok(vehicle);
        }
    }
}