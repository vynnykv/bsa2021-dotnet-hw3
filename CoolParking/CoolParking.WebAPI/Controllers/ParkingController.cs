﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService) =>
            _parkingService = parkingService;

        [Route("balance")]
        [HttpGet]
        public ActionResult<decimal> GetBalance() =>
            Ok(_parkingService.GetBalance());

        [Route("capacity")]
        [HttpGet]
        public ActionResult<int> GetCapacity() =>
            Ok(_parkingService.GetCapacity());

        [Route("freePlaces")]
        [HttpGet]
        public ActionResult<int> GetFreePlaces() =>
            Ok(_parkingService.GetFreePlaces());
    }
}