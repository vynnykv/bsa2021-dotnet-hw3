﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService) =>
            _parkingService = parkingService;

        [HttpGet]
        public ActionResult<IReadOnlyCollection<Vehicle>> GetVehiclesCollection() =>
            Ok(_parkingService.GetVehicles());

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehiclesById(string id)
        {
            if (!Vehicle.IsValidId(id))
                return BadRequest("Id is wrong");
            Vehicle vehicle = _parkingService.GetVehicleById(id);
            if (vehicle == null)
                return NotFound("Vehicle with this id is not found");
            return vehicle;
        }

        [HttpPost]
        public ActionResult<Vehicle> AddVehicle([FromBody] AddVehicleDto vehicleDto)
        {
            Vehicle vehicle;
            try
            {
                vehicle = new Vehicle(vehicleDto.Id, vehicleDto.VehicleType, vehicleDto.Balance);
                _parkingService.AddVehicle(vehicle);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Created("CoolParking/Models/Parking.Vehicles", vehicle);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicles(string id)
        {
            if (!Vehicle.IsValidId(id))
                return BadRequest("This id is wrong");
            Vehicle vehicle = _parkingService.GetVehicleById(id);
            if (vehicle == null)
                return NotFound("Vehicle with this id is not found");
            _parkingService.RemoveVehicle(id);
            return NoContent();
        }
    }
}