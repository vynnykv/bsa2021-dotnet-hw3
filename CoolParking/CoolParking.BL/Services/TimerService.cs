﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;
        public double Interval { get ; set; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }

        public void Start()
        {
            _timer = new Timer(Interval);
            _timer.Elapsed += Elapsed;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}