﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _parking;
        public decimal Balance { get; set; }
        public ICollection<Vehicle> Vehicles { get; private set; }
        public ICollection<TransactionInfo> Transactions { get; private set; }
        private Parking()
        {
            Vehicles = new List<Vehicle>();
            Transactions = new List<TransactionInfo>();
            Balance = Settings.InitialBalance;
        }
        public static Parking GetParking()
        {
            if (_parking == null)
            {
                _parking = new Parking();
            }
            return _parking;
        }
        public void Dispose()
        {
            _parking = null;
        }
    }
}
