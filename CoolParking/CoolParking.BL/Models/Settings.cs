﻿using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitialBalance { get => 0; }
        public static int ParkingCapacity { get => 10; }
        public static int PaymentCancellationPeriod { get => 5000; }
        public static int LoggingPeriod { get => 60000; }
        public static decimal FineCoefficient { get => 2.5m; }
        public static string LogPath { get => "Transactions.log"; }
        public static decimal GetTariff(VehicleType type)
        {
            return type switch
            {
                VehicleType.PassengerCar => 2,
                VehicleType.Truck => 5,
                VehicleType.Bus => 3.5m,
                VehicleType.Motorcycle => 1,
                _ => throw new ArgumentException()
            };
        }
    }
}