﻿using System;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; private set; }
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")]
        public DateTime TransactionTime { get; private set; }
        public TransactionInfo(string vehicleId, decimal sum, DateTime transactionTime)
        {
            #region checking conditions
            if (vehicleId is null)
                throw new ArgumentNullException(nameof(vehicleId), $"{nameof(vehicleId)} cannot be null");
            if (sum < 0)
                throw new ArgumentException("Sum must be greater than zero", nameof(sum));
            #endregion
            VehicleId = vehicleId;
            Sum = sum;
            TransactionTime = transactionTime;
        }
    }
}