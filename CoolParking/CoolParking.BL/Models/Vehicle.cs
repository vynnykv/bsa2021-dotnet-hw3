﻿using System;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly Random random = new Random();
        private static readonly Regex regex = new Regex(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
        [JsonPropertyName("id")]
        public string Id { get; private set; }
        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; private set; }
        [JsonPropertyName("balance")]
        public decimal Balance { get; internal set; }
        public Vehicle(
            string id,
            VehicleType vehicleType,
            decimal balance)
        {
            #region checking conditions
            if (id is null)
                throw new ArgumentNullException(nameof(id), $"{nameof(id)} cannot be null");
            if (!IsValidId(id))
                throw new ArgumentException("Id must be in ХХ-YYYY-XX format", nameof(id));
            if (balance < 0)
                throw new ArgumentException("Balance must be greater than zero", nameof(balance));
            if (!Enum.IsDefined(typeof(VehicleType), vehicleType))
                throw new ArgumentException("There's no such an vehicle type", nameof(vehicleType));
            #endregion
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            var identifier = new char[10];

            for (var i = 0; i < identifier.Length; i++)
            {
                if (i < 2 || i > 7)
                    identifier[i] = (char)random.Next('A', 'Z' + 1);
                else if(i == 2 || i == 7)
                    identifier[i] = '-';
                else
                    identifier[i] = (char)random.Next('0', '9' + 1);
            }
            return new string(identifier);
        }
        public static bool IsValidId(string id)
        {
            return regex.IsMatch(id);
        }
    }
}
